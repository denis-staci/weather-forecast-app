import React, { Component } from 'react';

import SunnyImage from '../assets/sunny.png';
import CloudyImage from '../assets/cloudy.png';
import RainyImage from '../assets/rainy.png';
import SnowyImage from '../assets/snowy.png';
import './WeatherReport.css';

const styles = {
  noMargin: {
    margin: '0px',
  },
  noTextDecoration: {    
    textDecoration: 'none',
  },
};


export default class WeatherReport extends Component {
  getPng = (weather) => {
    let weatherPng;

    switch(weather) {
      case 'sunny':
        weatherPng = SunnyImage;
        break;
      case 'cloudy':
        weatherPng = CloudyImage;
        break;
      case 'rainy':
        weatherPng = RainyImage;
        break;
      case 'snowy':
        weatherPng = SnowyImage;
        break;
      default:
        weatherPng = SunnyImage;
    }

    return weatherPng;
  }

  render() {
    const { highTemperature, lowTemperature, weather, weekday } = this.props;
    const weatherPng = this.getPng(weather);

    return (
      <div className="weather-card">
        <p style={{...styles.noMargin, ...styles.noTextDecoration}}>{weekday}</p>
        <div className="weather-card-body">
          <img
            src={weatherPng}
            width="50px"
            alt="sunny"
          />
        </div>
        <div className="weather-card-temp">
          <p style={{...styles.noMargin, ...styles.noTextDecoration}}>{highTemperature}</p>
          <p style={{...styles.noMargin, ...styles.noTextDecoration}}>{lowTemperature}</p>
        </div>
      </div>
    )
  }
}