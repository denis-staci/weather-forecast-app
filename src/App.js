import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import './App.css';
import WeatherReport from './components/WeatherReport';
// import Monday from './components/Monday';

function Index() {
  return (
    <p>Index</p>
  );
}

function Monday() {
  return (
    <p>Monday</p>
  );
}

function Tuesday() {
  return (
    <p>Tuesday</p>
  );
}

class App extends Component {
  state = {
    weatherData: [{
        weekday: 'Monday',
        weather: 'sunny',
        highTemperature: 87,
        lowTemperature: 59,
      }, {
        weekday: 'Tuesday',
        weather: 'rainy',
        highTemperature: 67,
        lowTemperature: 39,
      },
    ],
  }

  render() {
    const { weatherData } = this.state;

    return (
      <Router>
        <div className="App">
          {weatherData.map((
            { weather, highTemperature, lowTemperature, weekday },
            index,
          ) => (
            <Link to={`/${weekday}/`}>              
              <WeatherReport
                key={index}
                weekday={weekday}
                weather={weather}
                highTemperature={highTemperature}
                lowTemperature={lowTemperature}
              />
            </Link>
          ))}
        </div>
        <Route path="/" exact component={Index} />
        <Route path="/Monday" component={Monday} />
        <Route path="/Tuesday" component={Tuesday} />
      </Router>
    );
  }
}

export default App;
